import React, { Component } from "react";
import PropTypes from "prop-types";
class Home extends Component {
  render() {
    return <h1>Hello from home</h1>;
  }
}

Home.propTypes = {
  name: PropTypes.string,
  age: PropTypes.number,
  calcAge: PropTypes.func
};

export default Home;
